#include "RunLoadMonitor.h"

#include <fstream>
#include <iostream>
#include <string>


void print_hints()
{
  std::cout << "<app> <input_file>" << std::endl;
  std::cout << "-  <app>: this application;" << std::endl;
  std::cout << "-  <input_file>: path to file containing registrated time spans in format:" << std::endl;
  std::cout << "                 HH:MM HH:MM" << std::endl;
  std::cout << "                   ...      " << std::endl;
  std::cout << "                 HH:MM HH:MM" << std::endl;
}

RCode load_monitor_main(int argc, char** argv)
{
  const int expectedArgc = 2;
  if (argc != expectedArgc)
  {
    std::cout << "Unexpected number of arguments: " << argc << " but " << expectedArgc << " is required." << std::endl;
    print_hints();
    return RCode::BadArgument;
  }

  const std::string filePath = argv[1];
  std::ifstream inFile{filePath};
  if (!inFile)
  {
    std::cout << "Can't open file: " << filePath << std::endl;
    print_hints();
    return RCode::BadArgument;
  }

  return runLoadMonitor(inFile, std::cout);
}

int main(int argN, char** argV)
{
  const auto rcode = load_monitor_main(argN, argV);
  return static_cast<int>(rcode);
}
