#include "RunLoadMonitor.h"

#include <ParkingLotLib/LoadMonitor.h>
#include <ParkingLotLib/TimeSpanIO.h>


RCode runLoadMonitor(std::istream& io_in, std::ostream& o_out)
{
  using parking_lot::TimeSpanInputIterator;
  parking_lot::LoadMonitor lot;
  for (auto it = TimeSpanInputIterator{ io_in }; it != TimeSpanInputIterator{}; ++it)
  {
    lot.registerSpan(*it);
  }

  if (!io_in.eof())
  {
    o_out << "Badly formed file." << std::endl;
    return RCode::BadFile;
  }

  const auto maxLoadSpans = lot.maxLoadSpans(); 
  
  o_out << "Time spans of parking lot maximal load:" << std::endl;
  if (maxLoadSpans.empty())
  {
    o_out << "<empty>" << std::endl;
  }

  for (const auto& span : maxLoadSpans)
  {
    o_out << span << std::endl;
  }

  return RCode::Success;
}
