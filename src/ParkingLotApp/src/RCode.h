#pragma once


enum class RCode
{
  Success = 0,
  BadArgument = -1,
  BadFile = -2
};
