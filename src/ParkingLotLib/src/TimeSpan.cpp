#include <ParkingLotLib/TimeSpan.h>

#include <ctime>
#ifdef WIN32
#define timegm _mkgmtime
#endif
#include <iostream>
#include <iomanip>
#include <istream>


namespace
{

using parking_lot::TTime;
using parking_lot::TimeSpan;

}

std::istream& parking_lot::operator>>(std::istream& o_istream, TimeSpan& o_timeSpan)
{
  using std::chrono::system_clock;
  static const char* inFormat = "%H:%M";

  time_t tts[2]{};

  for (time_t& tt : tts)
  {
    std::time_t ttTmp{};
    std::tm* tm = std::gmtime(&ttTmp);
    o_istream >> std::get_time(tm, inFormat);
    tt = timegm(tm);
  }

  if (!o_istream.fail())
  {
    o_timeSpan.start = system_clock::from_time_t(tts[0]).time_since_epoch();
    o_timeSpan.end = system_clock::from_time_t(tts[1]).time_since_epoch();
  }

  return o_istream;
}

std::ostream& parking_lot::operator<<(std::ostream& o_ostream, const TimeSpan& i_timeSpan)
{
  using std::chrono::system_clock;
  const auto startTimeT = system_clock::to_time_t(system_clock::time_point{i_timeSpan.start});
  const auto endTimeT = system_clock::to_time_t(system_clock::time_point{i_timeSpan.end});

  o_ostream << std::put_time(std::gmtime(&startTimeT), "%H:%M ");
  o_ostream << std::put_time(std::gmtime(&endTimeT), "%H:%M");
  return o_ostream;
}

TimeSpan::TimeSpan(const TTime& i_start, const TTime& i_end)
  : start{i_start}
  , end{i_end}
{

}

bool TimeSpan::operator==(const TimeSpan& i_other) const 
{
  return this->start == i_other.start && this->end == i_other.end;
}
