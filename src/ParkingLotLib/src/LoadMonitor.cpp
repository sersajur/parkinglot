#include <ParkingLotLib/LoadMonitor.h>


using namespace parking_lot;

bool LotLoad::operator==(const LotLoad& i_other) const
{
  return time == i_other.time && carsNumber == i_other.carsNumber;
}

void LoadMonitor::registerChangeTime(const TTime& i_time, int i_change)
{
  auto it = m_timeOfChanges.find(i_time);
  if (it != m_timeOfChanges.end())
  {
    it->second += i_change;
    if (it->second == 0)
    {
      m_timeOfChanges.erase(it);
    }
  }
  else
  {
    m_timeOfChanges[i_time] = i_change;
  }
}

void LoadMonitor::registerSpan(const TimeSpan& i_time)
{
  registerChangeTime(i_time.start, 1);
  registerChangeTime(i_time.end, -1);
}

std::vector<TimeSpan> LoadMonitor::maxLoadSpans() const
{
  auto it = m_timeOfChanges.cbegin();
  if (it == m_timeOfChanges.cend())
  {
    return {};
  }

  std::vector<TimeSpan> maxLoadTimes;
  
  TTime startTime = it->first;
  TTime endTime = it->first;
  size_t curLoad = it->second;
  size_t maxLoad = 0;
  while (++it != m_timeOfChanges.cend())
  {
    endTime = it->first;

    if (curLoad > maxLoad)
    {
      maxLoadTimes.clear();
      maxLoad = curLoad;
    }

    if (curLoad == maxLoad)
    {
      maxLoadTimes.emplace_back(startTime, endTime);
    }

    startTime = endTime;
    curLoad += it->second;
  }

  return maxLoadTimes; 
}

LotLoadIterator LoadMonitor::begin() const
{
  return LotLoadIterator{m_timeOfChanges.begin()};
}

LotLoadIterator LoadMonitor::end() const
{
  return LotLoadIterator{m_timeOfChanges.end()};
}

LotLoadIterator::LotLoadIterator(std::map<TTime, long long>::const_iterator i_it)
  : m_it{i_it}
{
}

LotLoadIterator& LotLoadIterator::operator++()
{
  m_currentLoad += m_it->second;
  ++m_it;
  return *this;
}

bool LotLoadIterator::operator!=(const LotLoadIterator& i_other) const
{
  return m_it != i_other.m_it;
}

LotLoad LotLoadIterator::operator*() const
{
  return LotLoad{m_it->first, m_it->second + m_currentLoad};
}
