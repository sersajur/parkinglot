#pragma once

#include <ParkingLotLib/TimeSpan.h>

#include <iterator>


namespace parking_lot
{

using TimeSpanInputIterator = std::istream_iterator<TimeSpan>;

}
