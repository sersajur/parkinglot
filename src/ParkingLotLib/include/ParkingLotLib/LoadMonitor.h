#pragma once

#include <ParkingLotLib/TimeSpan.h>

#include <iterator>
#include <map>
#include <vector>


namespace parking_lot
{

struct LotLoad
{
  TTime time;
  long long carsNumber;
  bool operator==(const LotLoad& i_lhs) const;
};


class LotLoadIterator;

/// Provides monitoring of parking lot load.
class LoadMonitor
{
public:
  // Registers time span when single car is parked.
  // Notice, in case of zero duration LoadMonitor will ignore registration.
  void registerSpan(const TimeSpan& i_time);
  
  // Returns time spans of parking lot maximum load.
  std::vector<TimeSpan> maxLoadSpans() const;

  LotLoadIterator begin() const;
  LotLoadIterator end() const;

private:
  void registerChangeTime(const TTime& i_time, int i_change);
  std::map<TTime, long long> m_timeOfChanges;
};

class LotLoadIterator: public std::iterator<std::forward_iterator_tag, LotLoad>
{
  friend class LoadMonitor;
public:
  LotLoadIterator& operator++();
  bool operator!=(const LotLoadIterator& i_other) const;
  LotLoad operator*() const;

private:
  LotLoadIterator(std::map<TTime, long long>::const_iterator i_it);
  std::map<TTime, long long>::const_iterator m_it;
  long long m_currentLoad = 0;
};

}
