#pragma once

#include <chrono>
#include <istream> 

namespace parking_lot
{
	
using TTime = std::chrono::system_clock::time_point::duration;

struct TimeSpan
{
  TimeSpan() = default;
  TimeSpan(const TimeSpan&) = default;
  TimeSpan& operator=(const TimeSpan&) = default;
  TimeSpan(const TTime& i_start, const TTime& i_end);
  bool operator==(const TimeSpan& i_other) const;

  TTime start;
  TTime end;

  friend std::istream& operator>>(std::istream& o_istream, parking_lot::TimeSpan& o_timeSpan);
  friend std::ostream& operator<<(std::ostream& o_ostream, const parking_lot::TimeSpan& i_timeSpan);
};

}
