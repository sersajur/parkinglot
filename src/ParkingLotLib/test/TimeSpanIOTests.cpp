#include <ParkingLotLib/TimeSpanIO.h>

#include <ParkingLotLib/TimeSpan.h>

#include <gtest/gtest.h>

#include <ctime>
#ifdef WIN32
#define timegm _mkgmtime
#endif

#include <chrono>
#include <sstream>
#include <vector>


namespace
{
using TimeSpan = parking_lot::TimeSpan;
using TimeSpanInputIterator = parking_lot::TimeSpanInputIterator;

using namespace std::chrono_literals;
}

TEST(ReadTimeSpan, ShouldSucceedOnValidData)
{
  const std::string s = "12:01 13:17";
  std::stringstream sstream{s};

  const auto expected = TimeSpan{12h + 1min, 13h + 17min};

  TimeSpan actual;
  sstream >> actual;
   
  ASSERT_FALSE(sstream.fail());
  EXPECT_EQ(expected, actual);
}

TEST(ReadTimeSpan, ShouldFailOnInvalidData1)
{
  const std::string s = "12?01 13:17";
  std::stringstream sstream{s};

  const auto expected = TimeSpan{5h + 55min, 5h + 55min};

  TimeSpan actual = expected;
  sstream >> actual;

  ASSERT_TRUE(sstream.fail());
  EXPECT_EQ(expected, actual);
}

#ifdef WIN32 // TODO: report a bug and find workaround
TEST(ReadTimeSpan, DISABLED_ShouldFailOnInvalidData2)
#else
TEST(ReadTimeSpan, ShouldFailOnInvalidData2)
#endif
{
  const std::string s = "123:01 13:17";
  std::stringstream sstream{s};

  const auto expected = TimeSpan{55h + 55min, 55h + 55min};

  TimeSpan actual = expected;
  sstream >> actual;

  ASSERT_TRUE(sstream.fail());
  EXPECT_EQ(expected, actual);
}

TEST(ReadTimeSpan, ShouldFailOnInvalidData3)
{
  const std::string s = "123:0113:17";
  std::stringstream sstream{s};

  const auto expected = TimeSpan{55h + 55min, 55h + 55min};

  TimeSpan actual = expected;
  sstream >> actual;

  ASSERT_TRUE(sstream.fail());
  EXPECT_EQ(expected, actual);
}

TEST(ReadTimeSpanList, ShouldSucceedOnValidData)
{
  std::stringstream sstream;
  sstream << "12:01 13:17" << std::endl;
  sstream << "03:04 11:56" << std::endl;

  std::vector<TimeSpan> expected{TimeSpan{12h + 1min, 13h + 17min}, TimeSpan{3h + 4min, 11h + 56min}};

  std::vector<TimeSpan> actual{TimeSpanInputIterator{sstream}, TimeSpanInputIterator{}};
  
  ASSERT_TRUE(sstream.eof());
  EXPECT_EQ(expected, actual);
}

TEST(ReadTimeSpanList, ShouldFailOnInvalidData1)
{
  std::stringstream sstream;
  sstream << "12:01 13:17" << std::endl;
  sstream << "03:04 11?56" << std::endl;

  std::vector<TimeSpan> expected{TimeSpan{12h + 1min, 13h + 17min}};

  std::vector<TimeSpan> actual{TimeSpanInputIterator{sstream}, TimeSpanInputIterator{}};

  ASSERT_TRUE(sstream.fail());
  ASSERT_FALSE(sstream.eof());
  EXPECT_EQ(expected, actual);
}

TEST(ReadTimeSpanList, ShouldFailOnInvalidData2)
{
  std::stringstream sstream;
  sstream << "12:01 13:17" << std::endl;
  sstream << "03:04 11:5?" << std::endl;

  std::vector<TimeSpan> expected{TimeSpan{12h + 1min, 13h + 17min}, TimeSpan{3h + 4min, 11h + 5min}};

  std::vector<TimeSpan> actual{TimeSpanInputIterator{sstream}, TimeSpanInputIterator{}};

  ASSERT_TRUE(sstream.fail());
  ASSERT_FALSE(sstream.eof());
  EXPECT_EQ(expected, actual);
}

TEST(WriteTimeSpan, ShouldWrite)
{
  using namespace std::chrono_literals;
  const auto span = TimeSpan{ 3h + 10min, 13h + 5min };

  const std::string expected = "03:10 13:05";

  std::stringstream sstr;
  sstr << span;
  const auto actual = sstr.str();

  EXPECT_EQ(expected, actual);
}

TEST(StdBasis, TimeRead)
{
  using std::chrono::system_clock;
  using namespace std::chrono_literals;

  std::string s = "13:45";
  std::stringstream sstr{s};
  const auto expectedTp = system_clock::time_point{13h + 45min};

  const char* timeFormat = "%H:%M";
  std::time_t ttTmp{};
  std::tm* tm = std::gmtime(&ttTmp);
  sstr >> std::get_time(tm, timeFormat);
  auto tt = timegm(tm);
  const auto actualTp = system_clock::from_time_t(tt);

  EXPECT_EQ(expectedTp, actualTp);
}

TEST(StdBasis, TimeWrite)
{
  std::stringstream sstr;

  using namespace std::chrono_literals;
  const auto d = std::chrono::system_clock::duration{3h + 2min};
  const auto tp = std::chrono::system_clock::time_point(d);
  const auto tt = std::chrono::system_clock::to_time_t(tp);
  const auto lt = std::gmtime(&tt);
  sstr << std::put_time(lt, "%H:%M");

  const std::string expected = "03:02";
  const auto actual = sstr.str();

  EXPECT_EQ(expected, actual);
}
