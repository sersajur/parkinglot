#include <ParkingLotLib/LoadMonitor.h>

#include <gtest/gtest.h>

#include <chrono> 


using namespace parking_lot;

TEST(LoadMonitor, ShouldReturnIntersectionOfTwoTimeRanges)
{
  using namespace std::chrono_literals;

  LoadMonitor lot;
  lot.registerSpan({ 10min, 20min });
  lot.registerSpan({ 15min, 30min });
  const std::vector<TimeSpan> expected = {{15min, 20min}};

  const std::vector<TimeSpan> actual = lot.maxLoadSpans();

  EXPECT_EQ(expected, actual);
}

TEST(LoadMonitor, ShouldReturnAllTimeDomainOfSingleRegistration)
{
  using namespace std::chrono_literals;

  LoadMonitor lot;
  lot.registerSpan({ 10min, 20min });
  const std::vector<TimeSpan> expected = { {10min, 20min} };

  const std::vector<TimeSpan> actual = lot.maxLoadSpans();

  EXPECT_EQ(expected, actual);
}

TEST(LoadMonitor, ShouldReturnAllTimeDomainOfSeveralRegistration)
{
  using namespace std::chrono_literals;

  LoadMonitor lot;
  lot.registerSpan({ 10min, 40min });
  lot.registerSpan({ 10min, 20min });
  lot.registerSpan({ 20min, 30min });
  lot.registerSpan({ 30min, 40min });
  const std::vector<TimeSpan> expected = { {10min, 40min} };

  const std::vector<TimeSpan> actual = lot.maxLoadSpans();

  EXPECT_EQ(expected, actual);
}

TEST(LoadMonitor, ShouldIgnoreZeroDurationRegistration)
{
  using namespace std::chrono_literals;

  LoadMonitor lot;
  lot.registerSpan({ 10min, 10min });
  const std::vector<TimeSpan> expected = {};

  const std::vector<TimeSpan> actual = lot.maxLoadSpans();

  EXPECT_EQ(expected, actual);
}

TEST(LoadMonitor, ShouldReturnValidChartData)
{
  using namespace std::chrono_literals;

  LoadMonitor lot;
  lot.registerSpan({ 0min, 20min });
  lot.registerSpan({ 10min, 50min });
  lot.registerSpan({ 40min, 50min });
  lot.registerSpan({ 45min, 50min });
  const std::vector<LotLoad> expected =
  {
    {0min, 1},
    {10min, 2},
    {20min, 1},
    {40min, 2},
    {45min, 3},
    {50min, 0}
  };

  const std::vector<LotLoad> actual{lot.begin(), lot.end()};

  EXPECT_EQ(expected, actual);
}
